% Es 1.1
% search(Elem, List)
search(X, [X|_]).
search(X, [_|Xs]) :- search(X, Xs).

% Es 1.2
search2(X, [X,X|_]).
search2(X, [_|Xs]) :- search2(X, Xs).

% Es 1.3
% search_two(Elem, List), if List contains Elem 2 times but not consequentially
search_two(X, [X,_,X|Xs]).
search_two(X, [_|Xs]) :- search_two(X, Xs).

% Es 1.4
% search_anytwo(Elem, List), if the List contains Elem 2 times anywhere
count_elem(X, [], 0).
count_elem(X, [H|Xs], N) :- H \= X, count_elem(X, Xs, N).
count_elem(X, [X|Xs], N) :- count_elem(X, Xs, R), N is R + 1.

search_anytwo(X, List) :- count_elem(X, List, 2).