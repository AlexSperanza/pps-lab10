% Es 3.1
% same(List1, List2), if List1 and List2 are exactly the same
same([], []).
same([X|Xs], [X|Ys]) :- same(Xs, Ys).

% Es 3.2
% all_bigger(List1, List2), if 1 by 1 the elements of List1 are bigger than the elements of List2
all_bigger([X|[]], [Y|[]]) :- X > Y.
all_bigger([X|Xs], [Y|Ys]) :- X > Y, all_bigger(Xs, Ys).

% Es 3.3
% sublist(List1, List2), if all of the elements of List1 are contained in List2
search(X, [X|_]).
search(X, [_|Xs]) :- search(X, Xs).

sublist([], L).
sublist([X|Xs], L) :- search(X, L), sublist(Xs, L).
