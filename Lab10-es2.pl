% Es 2.1
% size(List, Size), Size contains the number of elements of a list
size([],0).
size([_|T], M) :- size(T, N), M is N + 1.

% Es 2.2
% relational size(List, Size), Size contains the number of elements of List
% Size is in the form: zero, s(zero), s(s(zero)), ... to represent respectively 0, 1, 2, ...
size_r([], zero).
size_r([H|T], s(R)) :- size_r(T, R).

% Es 2.3
% sum(List, Variable), Variable contains the arithmetic sum of the elements of List
sum([], 0).
sum([H|T], N) :- sum(T, R), N is R + H.

% Es 2.4
% average(List, Average), Average contains the arithmetic average of the element of List
% on an empty list it gives an exception
average_short([], 0).
average_short(L, A) :- size(L, C), sum(L,S), A is S/C.

average(L, A) :- average(L, 0, 0, A).
average([], C, S, A) :- A is S/C.
average([X|Xs], C, S, A) :- C2 is C + 1, S2 is S + X, average(Xs, C2, S2, A).

% Es 2.5
% max(List, Max), Max contains the arithmetic maximum of List
max([X|Xs], M) :- max(Xs, M, X).
max([], M, TM) :- M is TM.
max([X|Xs], M, TM) :- X >= TM, max(Xs, M, X).
max([X|Xs], M, TM) :- X < TM, max(Xs, M, TM).

% Es 2.6
% max_min(List, Max, Min)
% Max contains the maximum value of List
% Min contains the minimum value of List

min([X|Xs], M) :- min(Xs, M, X).
min([], M, TM) :- M is TM.
min([X|Xs], M, TM) :- X < TM, min(Xs, M, X).
min([X|Xs], M, TM) :- X >= TM, min(Xs, M, TM).

max_min(L, Max, Min) :- max(L, Max), min(L, Min).
