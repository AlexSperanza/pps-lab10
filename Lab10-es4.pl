% Es 4.1
% seq(N, List), List is a sequence of N '0's
seq(0, []).
seq(N, [0|T]) :- N > 0, N2 is N - 1, seq(N2, T).

% Es 4.2
% seqR(N, List), List is a sequence of numbers from N to 0.
seqR(0, [0]).
seqR(N, [N|T]) :- N > 0, N2 is N - 1, seqR(N2, T).

% Es 4.3
% seqR2(N, List), list is a sequence of number from 0 to N.
seqR2(N, N, [N]).
seqR2(N, L) :- seqR2(N, 0, L).
seqR2(N, M, [M|T]) :- M < N, M2 is M + 1, seqR2(N, M2, T).

% last(List1, X, List2), List 2 is List1 with X appended
last([], Y, [Y]).
last([X|Xs], Y, [X|R]) :- last(Xs, Y, R).

% Es 4.4
% inv(List1, List2), List2 is List1 with reversed elements
inv([], []).
inv([X|Xs], R) :- inv(Xs, L), last(L, X, R).

% Es 4.5
% double(List1, List2), List2 is the concatenation [List1,List1]
double(List1, R) :- double(List1, List1, R).
double(List1, [], List1).
double(List1, [X|Xs], R) :- last(List1, X, L), double(L, Xs, R).

% appendList(List1, List2, Result), Result is [List1, List2]
appendList(List1, [], List1).
appendList(List1, [X|Xs], List2) :- last(List1, X, R), appendList(R, Xs, List2).

% Es 4.6
% times(List1, N, List2), List2 is the concatenation N times of List1
times(List1, BaseList, N, N, List1).
times(List1, BaseList, I, N, List2) :- I < N, appendList(List1, BaseList, Result), I2 is I + 1, times(Result, BaseList, I2, N, List2).
times(List1, N, List2) :- times(List1, List1, 1, N, List2).

% Also -> double(List, R) :- times(List, 2, R).

% Es 4.7
% proj(List1, List2), List1 is a list of lists, List2 is a list with the first element of the lists inside List1
proj([], TempList, TempList).
proj([[X|_]|XXs], TempList, List2) :- last(TempList, X, Result), proj(XXs, Result, List2).
proj(List1, List2) :- proj(List1, [], List2).
